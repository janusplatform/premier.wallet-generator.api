'use strict';

const Web3 = require('web3');

const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = chai;

const server = require('../bin/server');

chai.use(chaiHttp);

it('wallets is valid', (done) => {

    let resultValidation = new Array();
    let req = { body: { quantity: 100 } };

    chai.request(server).post('/').send(req.body).end((err, res) => {

        for (let i = 0; i < res.body.result.length; i++) {
            let address = res.body.result[i].address;

            resultValidation.push(Web3.utils.isAddress(address));
        }

        const isValid = resultValidation.some(a => a === false);

        expect(res).to.have.status(201);
        expect(res.body.success).to.equals(true);
        expect(isValid).to.equals(false);
        done();
    });
});