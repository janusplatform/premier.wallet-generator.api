const { Wallet } = require('ethers');
const provider = require('./provider');

function fromPrivateKey(privateKey) {
  return new Wallet(privateKey, provider);
}

module.exports = {
  fromPrivateKey,
};
