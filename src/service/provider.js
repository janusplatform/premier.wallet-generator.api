const process = require('process');
const assert = require('assert');
const { InfuraProvider } = require('ethers/providers');

const { ETHEREUM_NETWORK_ID = '', INFURA_API_KEY = '' } = process.env;

const networkIdToNameMap = {
  1: 'homestead',
  4: 'rinkeby',
};

const networkName = networkIdToNameMap[ETHEREUM_NETWORK_ID];

assert.notStrictEqual(INFURA_API_KEY, '', 'Missing INFURA_API_KEY environment variable');
assert.notStrictEqual(ETHEREUM_NETWORK_ID, '', 'Missing ETHEREUM_NETWORK environment variable');
assert.notStrictEqual(
  networkName,
  undefined,
  `Unsupported Ethereum Network ${ETHEREUM_NETWORK_ID}. Supported networks are: ${Object.keys(networkIdToNameMap)}`
);

module.exports = new InfuraProvider(networkName, INFURA_API_KEY);
