const { Contract } = require('ethers');
const provider = require('./provider');
const contracts = require('../../contracts.json');

async function connectToContract({ tokenType }) {
  if (!contracts[tokenType]) {
    throw new Error(`Invalid tokenType parameter: ${tokenType}`);
  }

  const { chainId } = await provider.getNetwork();
  const { abi, addresses } = contracts[tokenType];

  const address = addresses[chainId];
  if (!address) {
    throw new Error(`Contract ${tokenType} not found in network ${chainId}`);
  }

  return new Contract(address, abi, provider);
}

module.exports = connectToContract;
