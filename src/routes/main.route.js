'use strict';
const process = require('process');
const express = require('express');
const router = express.Router();

const { ETHEREUM_NETWORK_ID } = process.env;

router.get('/', (req, res, next) => {
  res.status(200).send({
    title: 'Wallet Generation and Transaction Management API',
    version: '1.1.0',
    ethereum: {
      networkId: ETHEREUM_NETWORK_ID,
    },
  });
});

module.exports = router;
