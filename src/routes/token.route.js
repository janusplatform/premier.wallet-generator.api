'use strict';

const express = require('express');
const router = express.Router();
const useContract = require('../middlewares/use-contract');
const controller = require('../controllers/token-controller');

const useContractInvestment = useContract({ tokenType: 'PTI' });
const useContractYield = useContract({ tokenType: 'PTY' });

router.post('/burnPti', [useContractInvestment], controller.burn);
router.post('/mintPti', [useContractInvestment], controller.mint);

router.post('/burnPty', [useContractYield], controller.burn);
router.post('/mintPty', [useContractYield], controller.mint);

module.exports = router;
