'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/generate-wallet.controller');

router.post('/', controller.generate);

module.exports = router;
