const connectToContract = require('../service/connect-to-contract');
const { fromPrivateKey } = require('../service/create-wallet');

function createUseContract({ tokenType }) {
  return async function useContract(req, res, next) {
    try {
      const { privateKey } = req.body;
      const signer = fromPrivateKey(privateKey);
      req.contract = await connectToContract({ tokenType });
      req.contract = req.contract.connect(signer);

      next();
    } catch (err) {
      res.status(500).send({
        error: err.message,
      });
    }
  };
}

module.exports = createUseContract;
