'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const swaggerDoc = require('./swagger-doc.json');
const app = express();

const mainRoute = require('./routes/main.route');
const generateRoute = require('./routes/generate-wallet.route');
const tokenRoute = require('./routes/token.route');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.use('/', mainRoute);
app.use('/', generateRoute);
app.use('/token', tokenRoute);

module.exports = app;
