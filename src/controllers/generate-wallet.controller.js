'use strict';

const bip39 = require('bip39');
const hdkey = require('hdkey');
const ethUtil = require('ethereumjs-util');

exports.generate = (req, res) => {
  try {
    const quantity = parseInt(req.body.quantity);

    if (isNaN(quantity) || quantity <= 0) {
      res.status(400).send({
        success: false,
        error: 'invalid quantity',
      });
    }

    const wallets = [];

    for (let i = 1; i <= quantity; i++) {
      const mnemonic = bip39.generateMnemonic();

      const seed = bip39.mnemonicToSeedSync(mnemonic);
      const root = hdkey.fromMasterSeed(seed);
      const masterPrivateKey = root.privateKey.toString('hex');

      const addrNode = root.derive("m/44'/60'/0'/0/0");
      const pubKey = ethUtil.privateToPublic(addrNode._privateKey);
      const addr = ethUtil.publicToAddress(pubKey).toString('hex');
      const address = ethUtil.toChecksumAddress(addr);

      wallets.push({
        index: i,
        mnemonic: mnemonic,
        address: address,
        privateKey: masterPrivateKey,
      });
    }

    res.status(201).send({
      success: true,
      result: wallets,
    });
  } catch (e) {
    res.status(500).send({
      success: false,
      error: 'internal server error',
    });
  }
};
