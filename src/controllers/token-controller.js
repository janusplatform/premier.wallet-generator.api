exports.mint = async (req, res) => {
  try {
    const { contract } = req;
    const { account, amount } = req.body;

    const txn = await contract.mintTo(account, amount);
    await txn.wait(1);

    const { hash } = txn;

    res.status(200).send({
      hash,
    });
  } catch (err) {
    console.error(err);

    res.status(500).send({
      success: false,
      error: err.message,
    });
  }
};

exports.burn = async (req, res) => {
  try {
    const { contract } = req;
    const { account, amount } = req.body;

    const txn = await contract.burnFrom(account, amount);
    await txn.wait(1);

    const { hash } = txn;

    res.status(200).send({
      hash,
    });
  } catch (err) {
    console.error(err);

    res.status(500).send({
      success: false,
      error: err.message,
    });
  }
};
